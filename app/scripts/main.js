$(document).ready(function () {

  $('.m-taxi-phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });

  $('.open-m-taxi').on('click', function (e) {
    e.preventDefault();

    $('.m-taxi').toggle();
  });

  $('.m-taxi__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'm-taxi__centered') {
      $('.m-taxi').toggle();
    }
  });

  $('.m-taxi__add').on('click', function (e) {
    $(this).next().show();
    $(this).hide();
  });

  $('.m-taxi__input-link').on('click', function (e) {
    $(this).next().show();
    $(this).hide();
  });

  $('.m-taxi-validate-from-1').on('input', function () {
    if (!$(this).val() == '' && !$('.m-taxi-validate-from-2').val() == '' && !$('.m-taxi-validate-place-1').val() == '' && !$('.m-taxi-validate-place-2').val() == '' && $('.m-taxi-phone').inputmask('isComplete')) {
      $('.m-taxi__button').removeClass('m-taxi__button_disabled');
    } else {
      $('.m-taxi__button').addClass('m-taxi__button_disabled');
    }
  });

  $('.m-taxi-validate-from-2').on('input', function () {
    if (!$(this).val() == '' && !$('.m-taxi-validate-from-1').val() == '' && !$('.m-taxi-validate-place-1').val() == '' && !$('.m-taxi-validate-place-2').val() == '' && $('.m-taxi-phone').inputmask('isComplete')) {
      $('.m-taxi__button').removeClass('m-taxi__button_disabled');
    } else {
      $('.m-taxi__button').addClass('m-taxi__button_disabled');
    }
  });

  $('.m-taxi-validate-place-1').on('input', function () {
    if (!$(this).val() == '' && !$('.m-taxi-validate-place-2').val() == '' && !$('.m-taxi-validate-from-1').val() == '' && !$('.m-taxi-validate-form-2').val() == '' && $('.m-taxi-phone').inputmask('isComplete')) {
      $('.m-taxi__button').removeClass('m-taxi__button_disabled');
    } else {
      $('.m-taxi__button').addClass('m-taxi__button_disabled');
    }
  });

  $('.m-taxi-validate-place-2').on('input', function () {
    if (!$(this).val() == '' && !$('.m-taxi-validate-place-1').val() == '' && !$('.m-taxi-validate-from-1').val() == '' && !$('.m-taxi-validate-from-2').val() == '' && $('.m-taxi-phone').inputmask('isComplete')) {
      $('.m-taxi__button').removeClass('m-taxi__button_disabled');
    } else {
      $('.m-taxi__button').addClass('m-taxi__button_disabled');
    }
  });

  $('.m-taxi-phone').on('input', function () {
    if (!$('.m-taxi-validate-place-1').val() == '' && !$('.m-taxi-validate-place-2').val() == '' && !$('.m-taxi-validate-from-1').val() == '' && !$('.m-taxi-validate-from-2').val() == '' && $(this).inputmask('isComplete')) {
      $('.m-taxi__button').removeClass('m-taxi__button_disabled');
    } else {
      $('.m-taxi__button').addClass('m-taxi__button_disabled');
    }
  });

  $('.m-taxi-phone').on('input', function (e) {
    if ($(this).inputmask('isComplete')) {
      $('.m-taxi-check').show();
    } else {
      $('.m-taxi-check').hide();
    }
  });
});